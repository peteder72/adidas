#include "lightning.h"

// Program-lightning strike (a.k.a. channeling)
// Randomly flips one bit in every byte of input file

// Flip random bit in a char
void flipbyte_rand(uint8_t *c) {
    int random = rand() % 8;
    *c ^= 1 << random;
}

// Zzzap!
// Flips random bits in specified file
size_t lightning_strike(FILE *f) {

    uint8_t c = getc(f);
    size_t nbytes = 0;

    srand(time(NULL));

    while (!feof(f)) {
        // Go back one uint8_t...
        fseek(f, -1, SEEK_CUR);

        // ...and rewrite normal byte
        // with the damaged one
        flipbyte_rand(&c);
        fwrite(&c, 1, 1, f);
        nbytes++;

        c = getc(f);
    }

    // Flush stdout and reset file position
    fflush(f);
    fseek(f, 0, SEEK_SET);

    return nbytes;
}