#include "lightning.h"

// Flipping random bit inevery byte
// For the file, specified in an argument

int main(int argc, char **argv) {

    if (argc == 1) {
        printf("No file specified: exiting\n");
        exit(1);
    }

    // Open file to RW - important
    FILE *f = fopen(*(++argv), "r+");
    if (f == NULL) {
        printf("Could not open file %s\n", *argv);
    }

    size_t flipped = lightning_strike(f);

    fclose(f);

    printf("Successfully flipped %ld bytes\n", flipped);
}