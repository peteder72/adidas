#ifndef MAIN_H
#define MAIN_H
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <string.h>

#include "libadidas.h"
#include "utils.h"

// Enumerator for the mode
typedef enum {
    NONE,
    ENCODE,
    DECODE
} ad_mode_t;

#endif